package com.example.skindemo;

import android.os.Environment;

import java.io.File;

/**
 * @ClassName: Config
 * @Author: 史大拿
 * @CreateDate: 1/4/23$ 2:41 PM$
 * TODO
 */
public class Config {
    public static final String PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "skin-pack-making-debug.apk";
}
